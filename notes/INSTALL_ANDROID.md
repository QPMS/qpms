# Installing QPMS on Android/AOSP (-based) systems

Yes, it is possible. Basically all you need is a device capable of running [Termux](https://termux.com/) with enough memory to build everything.

The following instructions have been tested with Termux version 0.118.0 on
[e/OS/ R development build on a Samsung Galaxy S10e](https://doc.e.foundation/devices/beyond0lte/install)
([e-1.0-r-20220526188878-dev-beyond0lte](https://images.ecloud.global/dev/beyond0lte/)).
Presumably, they should work also on stock Android as well, but who
in their right mind would run all the spyware by Google & al.?

Physical keyboard or [remote access](https://wiki.termux.com/wiki/Remote_Access) is strongly recommended. :D

## Get Termux

Just [install the Termux app from F-Droid or Github as per instructions](https://github.com/termux/termux-app#f-droid).

Open Termux; the following steps of these instructions are basically
just commands you need to type in Termux.

## Install prerequisities from termux repositories

```
pkg install python3 cmake git clang build-essential binutils
```

## Build and install GSL

```
curl -O https://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/gsl/gsl-latest.tar.gz
tar xf gsl-latest.tar.gz
cd gsl-2.7.1
./configure --prefix=$PREFIX
make    # add -j4 or so to make it faster on multicore systems
make install
cd -
```

## Build and install OpenBLAS

```
git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS
make
make PREFIX=$PREFIX install
cd -
```

### Workaround for "broken" setup.py script

The goal is to fix `setup.py` so that it finds the correct libraries automatically, but in the meantime, you can use this workaround to get the Python part of QPMS installed:

```
ln -s $PREFIX/lib/libopenblas.so $PREFIX/LIB/liblapacke.so
```

## Build and install Numpy

(Successful build requires the `MATHLIB` environmental variable set, otherwise linking will fail; see https://wiki.termux.com/wiki/Python.)

```
MATHLIB=m pip3 install numpy
```

### Install Sympy

```
pip3 install sympy
```

## Build and install QPMS

```
git clone https://repo.or.cz/qpms.git
cd qpms
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX . # ugly, TODO make a separate build tree!
make install

python3 setup.py install
```

Hopefully, QPMS has installed successfully. At this point, you should be able
to import and use QPMS with some exceptions. First, there is some legacy code
in the `qpms.qpms_p` module (which is no longer imported automatically with
bare `import qpms`). You shouldn't need this unless you are trying to run some
historic Jupyter notebooks or other old custom scripts. It contains a scipy
dependence, and scipy is hard to get working in Android environment (as
it requires a Fortran compiler to build).

## Install matplotlib

If you try to run just `pip3 install matplotlib` in Termux, it might likely
fail when installing the `pillow` dependency.
First, according to [Termux wiki](https://wiki.termux.com/wiki/Python#Python_module_installation_tips_and_tricks),
pillow depends on `libpng` and `libjpeg-turbo`, which are fortunately
available in Termux packages.
Second, pillow instalation requires an additional environment variable
`LDFLAGS="-L/system/lib64"` to be set on 64-bit devices.

Hence:
```
pkg install libpng libjpeg-turbo
export LDFLAGS="-L/system/lib64" # on 64-bit devices
pip3 install matplotlib
```

After this step, you should be able to run the command-line scripts
from `misc/` directory and examples from `examples/` directory.
