"""
Constants and unit conversion, mostly for standalode usage.

Previously, QPMS used scipy.constants in the python parts.
Now the relevant ones are set explicitly here in order
to avoid dependency on the whole scipy (which needs a fortran
compiler to build.

The values are taken from scipy / "2018 CODATA recommended values".
They slightly differ from the constants in GSL that are used
in the C code. It would be desirable to use the same source,
hence the values here might be subject to change in future versions.
"""

epsilon_0 = ε_0 = 8.8541878128e-12  # ± 0.0000000013e-12      F m^-1
c = speed_of_light = 299792458.
eV = e = elementary_charge = 1.602176487e-19  # ± 0000000040e-19    C
hbar = ℏ = 1.054571800e-34  #  ± 0.000000013e-34    J s
mu_0 = μ_0 = 1.25663706212e-6  #  ± 0.00000000019 e-6     N A^-2

from math import pi
π = pi

μm = 1e-6
nm = 1e-9
# "SCUFF FREQUENCY UNIT"
SU = 3e14
SCUFF_OMEGAUNIT = SU

#freqs = freqs_weirdunits * c / μm

def nm2eV(lambda_nm, n = 1):
    return 2*π*c/(n * lambda_nm * nm) / (eV / ℏ)

def eV2nm(e_eV, n = 1):
    return 2*π*c/(n * e_eV * (eV/ℏ)) / nm

def SU2eV(e_SU):
    '''
    Scuff frequency units to eV.
    '''
    return e_SU * SU / (eV / ℏ)

def eV2SU(e_eV):
    return e_eV * (eV / ℏ) / SU

def SU2nm(e_SU, n = 1):
    return 2*π*c/(n * e_SU * SU) / nm

def nm2SU(lambda_nm, n = 1):
    return 2*π*c/(n * lambda_nm * nm) / SU

